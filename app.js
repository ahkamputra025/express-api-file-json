const express = require('express')
const bp =  require('body-parser')
const logger = require('morgan')
const fs = require('fs');

const port = 3001
const app = express()

app.use(bp.urlencoded({ extended: false }))
app.use(bp.json())

app.use(logger('dev'))

const dataPath = 'data.json';
const readFile = (
  callback,
  returnJson = false,
  filePath = dataPath,
  encoding = 'utf8'
) => {
  fs.readFile(filePath, encoding, (err, data) => {
    if(err){
      throw err;
    }
    callback(returnJson ? JSON.parse(data) : data);
  });
}

const writeFile = (
  fileData,
  callback,
  filePath = dataPath,
  encoding = 'utf8'
) => {
  fs.writeFile(filePath, fileData, encoding, err => {
    if(err){
      throw err;
    }
    callback();
  });
}

//GET
app.get('/users', (req, res) => {
  readFile(data => {
    res.send(data);
  });
});

//POST
app.post('/users', (req, res) => {
  readFile(data => {
    const NewID = Math.random(2);
    data[NewID] = req.body;

    writeFile(JSON.stringify(data, null, 2), () => {
      res.status(200).send('new user added');
    });
  }, true);
});

//PUT
app.put('/users/:id', (req, res) => {
  readFile(data => {
    const userID = req.params['id'];
    data[userID] = req.body;

    writeFile(JSON.stringify(data, null, 2), () => {
      res.status(200).send(`user id:${userID} updated`);
    });
  }, true);
});

//DELETE
app.delete('/users/:id', (req, res) => {
  readFile(data => {
    const userID = req.params['id'];
    delete data[userID];

    writeFile(JSON.stringify(data, null, 2), () => {
      res.status(200).send(`users id:${userID} removed`);
    });
  }, true)
});

app.listen(port, (err) => {
  if (err) {
    console.log('Error')
  } else {
    console.log('Running')
  }
})